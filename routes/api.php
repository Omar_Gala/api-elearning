<?php

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */
Route::group(['middleware' => 'auth:api'], function() {
    Route::get('perfiles', function(){
        return Api\Models\Perfiles::all();
    });
    Route::resource('cursos','CursosController');
    Route::resource('lecciones','LeccionesController');
    Route::resource('preguntas','PreguntasController');
    Route::resource('respuestas','RespuestasController');
    
    
    Route::group(['prefix' => 'session'], function () {
        // Para registrarse en el curso se enviará un post a sesion cursos
        Route::resource('estudiante','SesioncursosController');
        Route::get('lecciones/{id}','SesioncursosController@getLeccionesRegistradas');
        Route::post('responder','SesioncursosController@answerPregunta');
        Route::get('finalizarleccion/{id}','SesioncursosController@endLeccion');
    });
    
});


Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
    
    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});

