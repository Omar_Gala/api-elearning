<?php

namespace Api\Models;

use Illuminate\Database\Eloquent\Model;

class Perfiles extends Model
{
    //
    protected $table = "perfiles";
    protected $primary_key = "iId";
}
