<?php

namespace Api\Models;

use Illuminate\Database\Eloquent\Model;

class Pregunta extends Model
{
    //
    protected $table = 'preguntas';
    protected $primaryKey = "iId";

    // Relación one to many
    // Un pregunta Varias respuestas
    public function respuesta(){
        return $this->hasMany('Api\Models\Respuesta');
    }

    // Relación many to one
    public function leccion(){
        return $this->belongTo('Api\Models\Leccion','iId');
    }
}
