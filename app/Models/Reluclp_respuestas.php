<?php

namespace Api\Models;

use Illuminate\Database\Eloquent\Model;

class Reluclp_respuestas extends Model
{
    //
    protected $table = 'rel_uclp_respuestas';
    protected $primaryKey = "iId";
}
