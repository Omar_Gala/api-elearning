<?php

namespace Api\Models;

use Illuminate\Database\Eloquent\Model;

class Respuesta extends Model
{
    //
    protected $table = 'respuestas';
    protected $primaryKey = "iId";

    // Relación many to one
    public function pregunta(){
        return $this->belongTo('Api\Models\Pregunta','iId');
    }
}
