<?php

namespace Api\Models;

use Illuminate\Database\Eloquent\Model;

class Reluc_lecciones extends Model
{
    //
    protected $table = 'rel_uc_lecciones';
    protected $primaryKey = "iId";
}
