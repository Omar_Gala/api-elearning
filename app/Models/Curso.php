<?php

namespace Api\Models;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    //
    protected $table = 'cursos';
    protected $primaryKey = "iId";
    
    // Relación one to many
    // Un Curso Varias Lecciones
    public function lecciones(){
        return $this->hasMany('Api\Models\Leccion','iId');
    }

}
