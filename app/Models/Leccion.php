<?php

namespace Api\Models;

use Illuminate\Database\Eloquent\Model;

class Leccion extends Model
{
    //
    protected $table = 'lecciones';
    protected $primaryKey = "iId";

    // Relación one to many
    // Un leccion Varias preguntas
    public function preguntas(){
        return $this->hasMany('Api\Models\Pregunta');
    }

    // Relación many to one
    public function curso(){
        return $this->belongsTo('Api\Models\Curso','iId');
    }

} 
