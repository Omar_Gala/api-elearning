<?php

namespace Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Api\Models\Pregunta;
use Api\Models\Respuesta;

class PreguntasController extends Controller
{
    private $response = [
        'success' => false,
        'message' => "",
        'data'    => null,
        'code'  => "",
        'error'   => ""
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $response = $this->response;

        $preguntas = Pregunta::all();
        
        if(count($preguntas) > 0){ 
            $response['success'] = true;
            $response['message'] = "Consulta exitosa";
            $response['data'] = $preguntas;
            $response['code'] = 200;
        }
        else
        {
            $response['success'] = true;
            $response['message'] = "Se devolvió una lista vacía";            
            $response['code'] = 200;
        }
   
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //
         $response = $this->response;

         // Reglas de validación
         $rules = [
             'pregunta' => 'bail|required|max:250',
             'pregunta' => 'unique:mysql.preguntas,cPregunta',
             'id_leccion' => 'bail|required',
             'puntos' => 'bail|required|integer'
         ];  
             
         // Instancia para validar en base a las reglas
         $validator = new Validator();
 
         // Obtiene el resultado de la validación
         // Si es true devuelve los errores
         $validator = $validator::make($request->all(), $rules);
         
         if ($validator->fails()) {
             $response['success'] = false;
             $response['message'] = 'Error! Algo salió mal al intentar crear un curso.';
             $response['error']    = $validator->errors()->all();
             return $response;            
         }        
        
         try {            
             DB::beginTransaction();
 
             $pregunta = new Pregunta();
             $pregunta->cPregunta = $request->input('pregunta');
             $pregunta->iFk_id_leccion = $request->input('id_leccion');

            // Solo uno puede estar seleccionado o puede ser enviado
            // opción múltiple una respuesta correcta
            if($request->input('opcionmultiple_unarespuesta') != null){
                if($request->input('opcionmultiple_unarespuesta'))
                {
                    $pregunta->bOpcion_multiple_una_resp = 1;
                }
                else
                {
                    $pregunta->bOpcion_multiple_una_resp = 0;
                }                
            }
            // opción múltiple mas de una respuestsa correcta
            if($request->input('opcionmultiple_masdeunarespuesta') != null){
                if($request->input('opcionmultiple_masdeunarespuesta'))
                {
                    $pregunta->bOpcion_multiple_varias_resp = 1;
                }
                else
                {
                    $pregunta->bOpcion_multiple_varias_resp = 0;
                }                
            }
            // opción múltiple todas las respuestas correctas
            if($request->input('opcionmultiple_todascorrecdtas') != null){
                if($request->input('opcionmultiple_todascorrecdtas'))
                {
                    $pregunta->bOpcion_multiple_todas_resp_correctas = 1;
                }
                else
                {
                    $pregunta->bOpcion_multiple_todas_resp_correctas = 0;
                }                 
            }

             $pregunta->iPuntos = $request->input('puntos');
             $pregunta->save();
 
             $response['data'] = $pregunta; 
             $response['success'] = true;
             $response['message'] = "Los datos de la pregunta se guardaron correctamente";
             $response['code'] = 200;                                  
             
         } catch (\Exception $e) {
             DB::rollBack();
             $response['success'] = false;
             $response['message'] = 'Error! Algo salió mal al intentar guardar los datos de la pregunta.';
             $response['code'] = 300;
             $response['error']   = $e->getMessage();
         } finally {
             DB::commit();                      
         }        
 
         return response()->json($response, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         //
         $response = $this->response;
         //
         if($id===0)
         {
             $response['success'] = false;
             $response['message'] = 'Error! Se requiere un id de pregunta diferente de cero (0)';            
             $response['code'] = 400;
             return response()->json($response,400);
         }

         $pregunta = new Pregunta();         
         $pregunta = $pregunta->find($id);

         if($pregunta == null){
            $response['success'] = false;
            $response['message'] = 'Error! No se encontró registro con id = '.$id;            
            $response['code'] = 400;
            return response()->json($response,400);
         }

         $response['success'] = true;
         $response['message'] = "Consulta exitosa";
         $response["code"] = 200;
         $response['data']['pregunta'] = $pregunta;
         $response['data']['respuestas'] = $pregunta->respuesta();
 
         return response()->json($response,200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $response = $this->response;

        // Reglas de validación
        $rules = [
            'pregunta' => 'bail|required|max:250',            
            'id_leccion' => 'bail|required',
            'puntos' => 'bail|required|integer'
        ];  
            
        // Instancia para validar en base a las reglas
        $validator = new Validator();

        // Obtiene el resultado de la validación
        // Si es true devuelve los errores
        $validator = $validator::make($request->all(), $rules);

        if($id===0)
        {
            $response['success'] = false;
            $response['message'] = 'Error! Se requiere un id de una pregunta diferente de cero (0)';            
            $response['code'] = 400;
            return response()->json($response,400);
        }
        
        if ($validator->fails()) {
            $response['success'] = false;
            $response['message'] = 'Error! Algo salió mal al intentar crear una pregunta.';
            $response['error']    = $validator->errors()->all();
            return $response;            
        }

        $pregunta = new Pregunta();
        $pregunta = $pregunta->where('cPregunta',$request->input('pregunta'));
        $pregunta = $pregunta->where('iFk_id_leccion',$request->input('id_leccion'));
        $pregunta = $pregunta->where('iId','!=',$id);
        $pregunta = $pregunta->first();

        if(count($pregunta) > 0){
            $response['success'] = false;
            $response['message'] = 'Error! El campo pregunta ya está en uso en otro registro.';
            $response['error']    = $validator->errors()->all();
            return $response;     
        }

        try {            
            DB::beginTransaction();

            $pregunta = new Pregunta();
            $pregunta = $pregunta->find($id);
            $pregunta->cPregunta = $request->input('pregunta');
            $pregunta->iFk_id_leccion  = $request->input('id_leccion');
            $pregunta->iPuntos = $request->input('puntos');

             // Solo uno puede estar seleccionado o puede ser enviado
            // opción múltiple una respuesta correcta
            if($request->input('opcionmultiple_unarespuesta') != null){
                if($request->input('opcionmultiple_unarespuesta'))
                {
                    $pregunta->bOpcion_multiple_una_resp = 1;
                    $pregunta->bOpcion_multiple_varias_resp = 0;
                    $pregunta->bOpcion_multiple_todas_resp_correctas = 0;
                }
                else
                {
                    $pregunta->bOpcion_multiple_una_resp = 0;
                }                
            }
            // opción múltiple mas de una respuestsa correcta
            if($request->input('opcionmultiple_masdeunarespuesta') != null){
                if($request->input('opcionmultiple_masdeunarespuesta'))
                {
                    $pregunta->bOpcion_multiple_varias_resp = 1;
                    $pregunta->bOpcion_multiple_una_resp = 0;
                    $pregunta->bOpcion_multiple_todas_resp_correctas = 0;
                }
                else
                {
                    $pregunta->bOpcion_multiple_varias_resp = 0;
                }                
            }
            // opción múltiple todas las respuestas correctas
            if($request->input('opcionmultiple_todascorrecdtas') != null){
                if($request->input('opcionmultiple_todascorrecdtas'))
                {
                    $pregunta->bOpcion_multiple_todas_resp_correctas = 1;
                    $pregunta->bOpcion_multiple_varias_resp = 0;
                    $pregunta->bOpcion_multiple_una_resp = 0;
                }
                else
                {
                    $pregunta->bOpcion_multiple_todas_resp_correctas = 0;
                }                 
            }

            $pregunta->save();

            $response['data'] = $pregunta; 
            $response['success'] = true;
            $response['message'] = "Los datos de la pregunta se guardaron correctamente";
            $response['code'] = 200;                                  
            
        } catch (\Exception $e) {
            DB::rollBack();
            $response['success'] = false;
            $response['message'] = 'Error! Algo salió mal al intentar guardar los datos de la pregunta.';
            $response['code'] = 300;
            $response['error']    = $e->getMessage();
        } finally {
            DB::commit();                      
        }        

        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = $this->response;
        //
        if($id===0)
        {
            $response['success'] = false;
            $response['message'] = 'Error! Se requiere un id de curso diferente de cero (0)';            
            $response['code'] = 400;
            return response()->json($response,400);
        }
        try {
            //code...
       
            $pregunta = Pregunta::find($id);
            if($pregunta == null){
                $response['success'] = false;
                $response['message'] = 'Error! No se encontró registro con id = '.$id;            
                $response['code'] = 400;
                return response()->json($response,400);
            }
            $pregunta->delete();
            
            $response['success'] = true;
            $response['message'] = "El registro ha sido borrado";
            $response["code"]    = 200;
            $response['data']    = $pregunta;
        } catch (\Exception $e) {
            $response['success'] = false;
            $response['message'] = "Algo salió mal al intentar borrar el registro id= ".$id;
            $response["code"]    = 400;
            $response['error']    = $e->getMessage();
        }

        return response()->json($response,200);

    }
}
