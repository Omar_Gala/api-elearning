<?php

namespace Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Api\Models\Respuesta;
use Api\Models\Pregunta;

class RespuestasController extends Controller
{
    private $response = [
        'success' => false,
        'message' => "",
        'data'    => null,
        'code'  => "",
        'error'   => ""
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         //
         $response = $this->response;

         $respuestas = Respuesta::all();
         
         if(count($respuestas) > 0){
             $response['success'] = true;
             $response['message'] = "Consulta exitosa";
             $response['data'] = $respuestas;
             $response['code'] = 200;
         }
         else
         {
             $response['success'] = true;
             $response['message'] = "Se devolvió una lista vacía";  
             $response['data'] = [];          
             $response['code'] = 200;
         }
    
         return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $response = $this->response;

        // Reglas de validación
        $rules = [
            'respuesta' => 'bail|required|max:250',  
            'id_pregunta' => 'bail|required'
        ];  
            
        // Instancia para validar en base a las reglas
        $validator = new Validator();

        // Obtiene el resultado de la validación
        // Si es true devuelve los errores
        $validator = $validator::make($request->all(), $rules);
        
        if ($validator->fails()) {
            $response['success'] = false;
            $response['message'] = 'Error! Algo salió mal al intentar crear un curso.';
            $response['error']    = $validator->errors()->all();
            return $response;            
        }      

        if($request->input('correcta') != null){            
            // Validamos pregunta: que opción tiene de respuestas
            $pregunta = new Pregunta();
            $id = intval($request->input('id_pregunta'));
            $pregunta = $pregunta->find($id);

            // 1) si tiene respuesta única, debe de prohibir que se quieran 
            // marcas más de 1 respuesta como correcta
            if($pregunta->bOpcion_multiple_una_resp == 1){
                // Reviso si ya existe alguna para este pregunta
                // si ya existe una como correcta devolver un 
                // error
                $respuesta_ = new Respuesta();
                $respuesta_ = $respuesta_->where('iFk_id_pregunta', $id);
                $respuesta_ = $respuesta_->where('iCorrecta', 1);
                $respuesta_ = $respuesta_->first();
                if($respuesta_ != null){
                    $response['success'] = false;
                    $response['message'] = "Error! Ya existe una respuesta correcta para esta pregunta.";
                    $response['code'] = 400;
                    return response()->json($response,400);
                }
            }
            
            // 2) si tiene mas de una respuesta, se permite que se 
            // marquen más de una respuescta como correcta
            if($pregunta->bOpcion_multiple_varias_resp == 1){
                // Se verifica si existe todas las respuestas
                // y se quiere marcar esta última como correcta
                // Notificar que no puede marcar todas como
                // correcdtas
                $respuesta_ = new Respuesta();
                $respuesta_ = $respuesta_->where('iFk_id_pregunta', $id);
                $respuesta_ = $respuesta_->where('iCorrecta', 0);
                $respuesta_ = $respuesta_->first();
                if($respuesta_ == null){
                    $response['success'] = false;
                    $response['message'] = "Error! Ya existe una o más respuestas correctas, esta respuesta debe ser incorrecta o cambiar el estatus de alguna de las anteriores.";
                    $response['code'] = 400;
                    return response()->json($response,400);
                }
            }   
            // 3) Si todas, en automáticas poner correcta en todas las respuestas
            // relacionadas
            // Se ejecutará dentro del try           
        }     
       
        
        try {            
            DB::beginTransaction();

            $respuesta = new Respuesta();
            $respuesta->cRespuesta = $request->input('respuesta');
            $respuesta->iFk_id_pregunta = $request->input('id_pregunta');
            
            if($request->input('correcta') != null){
                if($pregunta->bOpcion_multiple_todas_resp_correctas == 1){
                    // Aquí simplemente se verifique que todas estén
                    // marcadas y si esta última no está marcada
                    // se marca también
                    $respuesta_ = new Respuesta();
                    $respuesta_ = $respuesta_->where('iFk_id_pregunta', $id);  
                    $respuesta_ = $respuesta_->where('iCorrecta', 0);                  
                    $respuesta_ = $respuesta_->get();

                    if(count($respuesta_) > 0) {
                        foreach ($respuesta_ as $r) {
                            $obj = new Respuesta();
                            $obj = $obj->find($r->iId);
                            $obj->iCorrecta = 1;
                            $obj->save();
                        }
                    }                    
                }
                
                $respuesta->iCorrecta = 1;
            }      

            $respuesta->save();

            $response['data'] = $respuesta; 
            $response['success'] = true;
            $response['message'] = "Los datos de la respuesta se guardaron correctamente";
            $response['code'] = 200;                                  
            
        } catch (\Exception $e) {
            DB::rollBack();
            $response['success'] = false;
            $response['message'] = 'Error! Algo salió mal al intentar guardar los datos de la respuesta.';
            $response['code'] = 300;
            $response['error']   = $e->getMessage();
        } finally {
            DB::commit();                      
        }        

        return response()->json($response, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          //
          $response = $this->response;

          // Reglas de validación
          $rules = [
              'respuesta' => 'bail|required|max:250',  
              'id_pregunta' => 'bail|required'
          ];
              
          // Instancia para validar en base a las reglas
          $validator = new Validator();
  
          // Obtiene el resultado de la validación
          // Si es true devuelve los errores
          $validator = $validator::make($request->all(), $rules);

          if($id===0)
          {
                $response['success'] = false;
                $response['message'] = 'Error! Se requiere un id de una pregunta diferente de cero (0)';            
                $response['code'] = 400;
                return response()->json($response,400);
          }
          
          if ($validator->fails()) {
              $response['success'] = false;
              $response['message'] = 'Error! Algo salió mal al intentar crear un curso.';
              $response['error']    = $validator->errors()->all();
              return $response;            
          }      
  
          if($request->input('correcta') != null){            
              // Validamos pregunta: que opción tiene de respuestas
              $pregunta = new Pregunta();
              $idp = intval($request->input('id_pregunta'));
              $pregunta = $pregunta->find($idp);
  
              // 1) si tiene respuesta única, debe de prohibir que se quieran 
              // marcas más de 1 respuesta como correcta
              if($pregunta->bOpcion_multiple_una_resp == 1){
                  // Reviso si ya existe alguna para este pregunta
                  // si ya existe una como correcta devolver un 
                  // error
                  $respuesta_ = new Respuesta();
                  $respuesta_ = $respuesta_->where('iFk_id_pregunta', $idp);
                  $respuesta_ = $respuesta_->where('iCorrecta', 1);
                  $respuesta_ = $respuesta_->first();
                  if($respuesta_ != null){
                      $response['success'] = false;
                      $response['message'] = "Error! Ya existe una respuesta correcta para esta pregunta.";
                      $response['code'] = 400;
                      return response()->json($response,400);
                  }
              }
              
              // 2) si tiene mas de una respuesta, se permite que se 
              // marquen más de una respuescta como correcta
              if($pregunta->bOpcion_multiple_varias_resp == 1){
                  // Se verifica si existe todas las respuestas
                  // y se quiere marcar esta última como correcta
                  // Notificar que no puede marcar todas como
                  // correcdtas
                  $respuesta_ = new Respuesta();
                  $respuesta_ = $respuesta_->where('iFk_id_pregunta', $idp);
                  $respuesta_ = $respuesta_->where('iCorrecta', 0);
                  $respuesta_ = $respuesta_->first();
                  if($respuesta_ == null){
                      $response['success'] = false;
                      $response['message'] = "Error! Ya existe una o más respuestas correctas, esta respuesta debe ser incorrecta o cambiar el estatus de alguna de las anteriores.";
                      $response['code'] = 400;
                      return response()->json($response,400);
                  }
              }   
              // 3) Si todas, en automáticas poner correcta en todas las respuestas
              // relacionadas
              // Se ejecutará dentro del try           
          }              
          
          try {            
              DB::beginTransaction();
  
              $respuesta = new Respuesta();
              $respuesta = $respuesta->find($id);
              $respuesta->cRespuesta = $request->input('respuesta');
              $respuesta->iFk_id_pregunta = $request->input('id_pregunta');
              
              if($request->input('correcta') != null){
                  if($pregunta->bOpcion_multiple_todas_resp_correctas == 1){
                      // Aquí simplemente se verifique que todas estén
                      // marcadas y si esta última no está marcada
                      // se marca también
                      $respuesta_ = new Respuesta();
                      $respuesta_ = $respuesta_->where('iFk_id_pregunta', $idp);  
                      $respuesta_ = $respuesta_->where('iCorrecta', 0);                  
                      $respuesta_ = $respuesta_->get();
  
                      if(count($respuesta_) > 0) {
                          foreach ($respuesta_ as $r) {
                              $obj = new Respuesta();
                              $obj = $obj->find($r->iId);
                              $obj->iCorrecta = 1;
                              $obj->save();
                          }
                      }                    
                  }
                  
                  $respuesta->iCorrecta = 1;
              }      
  
              $respuesta->save();
  
              $response['data'] = $respuesta; 
              $response['success'] = true;
              $response['message'] = "Los datos de la respuesta se guardaron correctamente";
              $response['code'] = 200;                                  
              
          } catch (\Exception $e) {
              DB::rollBack();
              $response['success'] = false;
              $response['message'] = 'Error! Algo salió mal al intentar guardar los datos de la respuesta.';
              $response['code'] = 300;
              $response['error']   = $e->getMessage();
          } finally {
              DB::commit();                      
          }        
  
          return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = $this->response;
        //
        if($id===0)
        {
            $response['success'] = false;
            $response['message'] = 'Error! Se requiere un id de curso diferente de cero (0)';            
            $response['code'] = 400;
            return response()->json($response,400);
        }
        try {
            //code...
       
            $respuesta = Respuesta::find($id);
            if($respuesta == null){
                $response['success'] = false;
                $response['message'] = 'Error! No se encontró registro con id = '.$id;            
                $response['code'] = 400;
                return response()->json($response,400);
            }
            $respuesta->delete();
            
            $response['success'] = true;
            $response['message'] = "El registro ha sido borrado";
            $response["code"]    = 200;
            $response['data']    = $respuesta;
        } catch (\Exception $e) {
            $response['success'] = false;
            $response['message'] = "Algo salió mal al intentar borrar el registro id= ".$id;
            $response["code"]    = 400;
            $response['error']    = $e->getMessage();
        }

        return response()->json($response,200);
    }
}
