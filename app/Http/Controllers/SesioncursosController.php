<?php

namespace Api\Http\Controllers;

use Illuminate\Http\Request;
use Api\Models\Curso;
use Api\Models\Leccion;
use Api\Models\Pregunta;
use Api\Models\Respuesta;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Api\Models\Relu_cursos;
use Api\Models\Reluc_lecciones;
use Api\Models\Relucl_preguntas;
use Api\Models\Reluclp_respuestas;

class SesioncursosController extends Controller
{
    private $response = [
        'success' => false,
        'message' => "",
        'data'    => null,
        'code'  => "",
        'error'   => ""
    ];
    /**
     * Display a listing of the resource. 
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Para empesar, obtenemos los datos del usuario logueado y que sea estudiante
        $usr = $request->user();
        $id_usuario = $usr->id;
        $perfil = $usr->iFk_id_perfil;

        if($perfil != 3){
            $response['success'] = false;
            $response['message'] = 'Error! Acceso denegado. Solo pueden acceder estudiantes.';
            return response()->json($response);
        }

        $curso = new Curso();

        // Solo se muestran los cursos que puede ver el estudiante
        $cursos_usuario = new Relu_cursos();
        $cursos_usuario = $cursos_usuario->where('iFk_id_usuario',$id_usuario);
        $cursos_usuario = $cursos_usuario->orderBy('iId');
        $cursos_usuario = $cursos_usuario->get();

        if(count($cursos_usuario) > 0 ){
            // Es decir si hay 4 cursos, y no ha tomado ninguno, solo puede ver
            // el primer cursos
            // Si está tomando el primer cursos, no puede ver el siguiente hasta que finalice
            // y apruebe
            // Si finalizó y aprobó puede ver el siguiente curso
            // Puede repetir el curso
            $curso_pendiente = false;
            $cursos_concluidos = [];
            $response['data'] = [];   
            foreach ($cursos_usuario as $cu) {
                //array_push($response['data'],$cu);
                if(($cu->iFinalizado == 0 && $cu->iAprobado == 0) || 
                        ($cu->iFinalizado == 1 && $cu->iAprobado == 0))
                {
                    // no se muestra el siguiente
                    $curso_pendiente = true;
                    array_push($response['data'],$cu);                    
                }
                else
                {
                    array_push($response['data'],$cu);
                    array_push($cursos_concluidos,$cu);
                    // se muestra el siguiente curso inmediato diferente 
                    // a los ya cursados
                }               
                           
            }
            if(!$curso_pendiente){
                // objet curso nuevo inmediato que se necesita
                $obj_curso_inmediato  = new Curso();
                $obj_curso_inmediato  = $obj_curso_inmediato->orderBy("iId");
                foreach ($cursos_concluidos as $cc) {
                    $obj_curso_inmediato = $obj_curso_inmediato->where("iId","!=",$cc->ifk_id_curso);
                }
              
                $obj_curso_inmediato  = $obj_curso_inmediato->first();

                $obj_rel_user_curso = new Relu_cursos();                        
                $obj_rel_user_curso->ifk_id_curso = $obj_curso_inmediato->iId;
                $obj_rel_user_curso->iFk_id_usuario = 0;
                $obj_rel_user_curso->iFinalizado = 0;
                $obj_rel_user_curso->iAprobado = 0;
                $obj_rel_user_curso->iPuntos = 0;
                $obj_rel_user_curso->created_at = null;
                $obj_rel_user_curso->updated_at = null;
                $obj_rel_user_curso->iId = 0;	
                array_push($response['data'],$obj_rel_user_curso);
            }     

            $response['success']  = true;
            $response['message'] = 'Tiene acceso a los cursos ya tomados y el siguiente para registrarse';
            $response['code'] = 200;
        }
        else
        {
            // Se muestra el primer cursos sea aprobado o finalizado
            $curso = $curso->orderBy('iId');
            $curso = $curso->first();

            $obj_rel_user_curso = new Relu_cursos();                        
            $obj_rel_user_curso->ifk_id_curso = $curso->iId;
            $obj_rel_user_curso->iFk_id_usuario = 0;
            $obj_rel_user_curso->iFinalizado = 0;
            $obj_rel_user_curso->iAprobado = 0;
            $obj_rel_user_curso->iPuntos = 0;
            $obj_rel_user_curso->created_at = null;
            $obj_rel_user_curso->updated_at = null;
            $obj_rel_user_curso->iId = 0;	
          
            $response['success']  = true;
            $response['data'] = [];            
            array_push($response['data'],$obj_rel_user_curso);
            $response['message'] = 'Tiene acceso al primer curso para registrarse';
            $response['code'] = 200;
        }     
       /*  $token  = ;
        $response = $this->response;*/
        return response()->json($response); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Registrar la sesión de parte del estudiante
        //Recibiremos id del curso y obtenemos los datos del usuario.
        
        $response = $this->response;

        // Para empesar, obtenemos los datos del usuario logueado y que sea estudiante
        $usr = $request->user();
        $id_usuario = $usr->id;

        $rules = [
            'id_curso' => 'bail|required|integer'
        ];  
        // Instancia para validar en base a las reglas
        $validator = new Validator();

        // Obtiene el resultado de la validación
        // Si es true devuelve los errores
        $validator = $validator::make($request->all(), $rules);
        
        if ($validator->fails()) {
            $response['success'] = false;
            $response['message'] = 'Error! Algo salió mal al intentar registrar el curso.';
            $response['error']    = $validator->errors()->all();
            return $response;            
        }

        $obj = new Relu_cursos();
        $obj = $obj->where('iFk_id_usuario',$id_usuario);
        $obj = $obj->where('ifk_id_curso',$request->input('id_curso'));
        $obj = $obj->first();

        if($obj != null){
            $response['success'] = true;
            $response['message'] = 'Error! Ya existe un registro de este curso con el mismo usuario';
            $response['code'] = 400;
            return response()->json($response);
        }

        try {            
            DB::beginTransaction();

            $curso_usuario = new Relu_cursos();
            $curso_usuario->iFk_id_usuario = $id_usuario;
            $curso_usuario->ifk_id_curso   = $request->input('id_curso');
            $curso_usuario->save();

            //Creamos las lecciones de la sesión del curso
            //Obtenemos el arreglo de las lecciones del curso
            $lecciones = new Leccion();
            $lecciones = $lecciones->where('iFk_id_curso', $curso_usuario->ifk_id_curso);
            $lecciones = $lecciones->orderBy('iId');
            $lecciones = $lecciones->get();
           
            //Insertamos cada lección en la tabla 
            
            foreach ($lecciones as $l) {
                $uc_lecciones = new Reluc_lecciones();
                $uc_lecciones = $uc_lecciones->where('iFk_id_rel_user_curso',$curso_usuario->iId);
                $uc_lecciones = $uc_lecciones->where('iFk_id_leccion',$l->iId);
                $uc_lecciones = $uc_lecciones->first();
                if($uc_lecciones == null){
                    $obj = new Reluc_lecciones();
                    $obj->iFk_id_rel_user_curso = $curso_usuario->iId;
                    $obj->iFk_id_leccion = $l->iId;
                    $obj->save();
                    
                    $obj_guardado_id_leccion = $obj->iFk_id_leccion;
                    $obj_guardado_id = $obj->iId;
                    

                    // Guardamos las preguntas de la leccion creada
                    //Obtenemos la pregunta de la lección
                    $arr_preguntas = new Pregunta();
                    $arr_preguntas = $arr_preguntas->where('iFk_id_leccion',$obj_guardado_id_leccion);
                    $arr_preguntas = $arr_preguntas->get();
                    if($arr_preguntas!= null || count($arr_preguntas))
                    {
                        foreach ($arr_preguntas as $ap) {                            
                            $obj_ucl_preguntas_existentes = new Relucl_preguntas();
                            $obj_ucl_preguntas_existentes = $obj_ucl_preguntas_existentes->where("iFk_id_rel_uc_lecciones",$obj_guardado_id_leccion );
                            $obj_ucl_preguntas_existentes = $obj_ucl_preguntas_existentes->where("iFk_id_pregunta",$ap->iId);
                            $obj_ucl_preguntas_existentes = $obj_ucl_preguntas_existentes->first();

                            if($obj_ucl_preguntas_existentes==null){
                                $obj_new_ucl_preguntas = new Relucl_preguntas();
                                $obj_new_ucl_preguntas->iFk_id_rel_uc_lecciones = $obj_guardado_id;
                                $obj_new_ucl_preguntas->iFk_id_pregunta = $ap->iId;
                                $obj_new_ucl_preguntas->save();
                                
                                // Guardamos las respuestas
                                // las obtenemos primero
                                $respuestas_de_pregunta = new Respuesta();
                                $respuestas_de_pregunta = $respuestas_de_pregunta->where("iFk_id_pregunta",$obj_new_ucl_preguntas->iFk_id_pregunta);
                                $respuestas_de_pregunta = $respuestas_de_pregunta->get();                                

                                if($respuestas_de_pregunta != null){
                                    foreach ($respuestas_de_pregunta as $rp) {
                                        $uclp_respuestas = new Reluclp_respuestas();
                                        $uclp_respuestas = $uclp_respuestas->where('iFk_id_rel_ucl_preguntas',$obj_new_ucl_preguntas->iId);
                                        $uclp_respuestas = $uclp_respuestas->where('iFk_id_respuesta',$rp->iId);
                                        $uclp_respuestas = $uclp_respuestas->first();

                                        if($uclp_respuestas == null){
                                            $obj_uclp_respuestas = new Reluclp_respuestas();
                                            $obj_uclp_respuestas->iFk_id_rel_ucl_preguntas = $obj_new_ucl_preguntas->iId;
                                            $obj_uclp_respuestas->iFk_id_respuesta = $rp->iId;
                                            $obj_uclp_respuestas->save();
                                        }
                                    }
                                } 
                            }
                        }
                    }         

                }
            }
            //Obtenemos las preguntas de las lecciones
            //Insertamos en tabla
            //Obtenemos las respuesta por pregunta e insertamos en tabla

            //Creamos las preguntas de la sesión del curso y leccion

            //Creamos las respuestas de las preguntas de la seción del curso y lección

            $response['data'] = $curso_usuario; 
            $response['success'] = true;
            $response['message'] = "El registro del usuario al curso ha sido exitoso.";
            $response['code'] = 200;                                  
            
        } catch (\Exception $e) {
            DB::rollBack();
            $response['success'] = false;
            $response['message'] = 'Error! Algo salió mal al intengar registrar usuario al curso.';
            $response['code'] = 300;
            $response['error']    = $e->getMessage();
        } finally {
            DB::commit();                      
        }        

        return response()->json($response, 200);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Muestras el detalle del curso y las lecciones que lo conforman
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Obtener lecciones de curso registrado
     * @param  int  $id este es del registro
     */

    public function getLeccionesRegistradas($id){
        // Este va a devolver la lista de lecciones que puede ver
        // el estudiante y se va con todo el paquete de preguntas y respuestas
        // como lo pidió el frontend
        $response = $this->response;

        $lecciones_registradas_estudiante = []; // Este acumulará todos los registros 

        if($id===0)
        {
            $response['success'] = false;
            $response['message'] = 'Error! Se requiere un id de curso diferente de cero (0)';            
            $response['code'] = 400;
            return response()->json($response,400);
        }

        $relu_cursos        = new Relu_cursos();
        $reluc_lecciones    = new Reluc_lecciones();
        $relucl_preguntas   = new Relucl_preguntas();
        

        $relu_cursos = $relu_cursos->select(
            "rel_user_curso.iId as id_sesion_curso",
            "rel_user_curso.iFk_id_usuario as id_usuario",
            "rel_user_curso.ifk_id_curso as id_curso",
            'c.cNombre as nombre_curso',
            "rel_user_curso.iFinalizado as finalizado",
            "rel_user_curso.iAprobado as aprobado",
            "rel_user_curso.iPuntos as puntos_aprobatorios"
        );
        $relu_cursos = $relu_cursos->join('cursos as c','c.iId','=','rel_user_curso.ifk_id_curso');
        $relu_cursos = $relu_cursos->where('rel_user_curso.iId',$id);
        $relu_cursos = $relu_cursos->first();

        if($relu_cursos != null)
        {
            $lecciones_registradas_estudiante['id_sesion_curso'] = $relu_cursos->id_sesion_curso;
            $lecciones_registradas_estudiante['id_usuario'] = $relu_cursos->id_usuario;
            $lecciones_registradas_estudiante['id_curso'] = $relu_cursos->id_curso;
            $lecciones_registradas_estudiante['nombre_curso'] = $relu_cursos->nombre_curso;
            $lecciones_registradas_estudiante['finalizado'] = $relu_cursos->finalizado;
            $lecciones_registradas_estudiante['aprobado'] = $relu_cursos->aprobado;
            $lecciones_registradas_estudiante['puntos_aprobatorios'] = $relu_cursos->puntos_aprobatorios;
            $lecciones_registradas_estudiante['lecciones'] = [];
        };
        $reluc_lecciones = $reluc_lecciones->select(
            'rel_uc_lecciones.iId as id_leccion',
            'rel_uc_lecciones.iFk_id_rel_user_curso',
            'rel_uc_lecciones.iFk_id_leccion',
            'l.cNombre as nombre_leccion',
            'l.iFk_id_curso',
            'cu.cNombre as nombre_curso',
            'rel_uc_lecciones.iFinalizado as finalizado_leccion',
            'rel_uc_lecciones.iAprobado as aprobado_leccion',
            'rel_uc_lecciones.iPuntos as puntos_leccion'
        );
        $reluc_lecciones = $reluc_lecciones->join('lecciones as l','l.iId','=','rel_uc_lecciones.iFk_id_leccion');
        $reluc_lecciones = $reluc_lecciones->join('cursos as cu','cu.iId','=','l.iFk_id_curso');
        $reluc_lecciones = $reluc_lecciones->where('rel_uc_lecciones.iFk_id_rel_user_curso',$id);
        $reluc_lecciones = $reluc_lecciones->get();

        if($reluc_lecciones != null)
        {
            $arr_lecciones = [];
            foreach ($reluc_lecciones as $rl) {
                $arr_lecciones['id_leccion']=$rl->id_leccion;
                $arr_lecciones['iFk_id_rel_user_curso']=$rl->iFk_id_rel_user_curso;
                $arr_lecciones['iFk_id_leccion']=$rl->iFk_id_leccion;
                $arr_lecciones['nombre_leccion']=$rl->nombre_leccion;
                $arr_lecciones['iFk_id_curso']=$rl->iFk_id_curso;
                $arr_lecciones['nombre_curso']=$rl->nombre_curso;
                $arr_lecciones['iFinalizado_leccion']=$rl->finalizado_leccion;
                $arr_lecciones['aprobado_leccion']=$rl->aprobado_leccion;
                $arr_lecciones['puntos_leccion']=$rl->puntos_leccion;
                $arr_lecciones['preguntas']=[];
                
                //Obtenemos las preguntas por lección
                $relucl_preguntas = $relucl_preguntas->select(
                    'rel_ucl_preguntas.iId as id_pregunta',
                    'rel_ucl_preguntas.iFk_id_rel_uc_lecciones',
                    'rel_ucl_preguntas.iFk_id_pregunta',
                    'p.cPregunta as pregunta',
                    'p.bOpcion_multiple_una_resp',  
                    'p.bOpcion_multiple_varias_resp',
                    'p.bOpcion_multiple_todas_resp_correctas',
                    'p.iPuntos',
                    'p.iActivo as estatus_activo_pregunta'
                );
                $relucl_preguntas = $relucl_preguntas->join('preguntas as p','p.iId','=','rel_ucl_preguntas.iFk_id_pregunta');
                $relucl_preguntas = $relucl_preguntas->where('rel_ucl_preguntas.iFk_id_rel_uc_lecciones',$rl->id_leccion);
                $relucl_preguntas = $relucl_preguntas->get();
                
                $arr_preguntas = [];
                
                if($relucl_preguntas != null){
                    foreach ($relucl_preguntas as $rp) {
                        $arr_preguntas['id_pregunta'] = $rp->id_pregunta;
                        $arr_preguntas['iFk_id_rel_uc_lecciones'] = $rp->iFk_id_rel_uc_lecciones;
                        $arr_preguntas['iFk_id_pregunta'] = $rp->iFk_id_pregunta;
                        $arr_preguntas['pregunta'] = $rp->pregunta;
                        $arr_preguntas['bOpcion_multiple_una_resp'] = $rp->bOpcion_multiple_una_resp;
                        $arr_preguntas['bOpcion_multiple_varias_resp'] = $rp->bOpcion_multiple_varias_resp;
                        $arr_preguntas['bOpcion_multiple_todas_resp_correctas'] = $rp->bOpcion_multiple_todas_resp_correctas;
                        $arr_preguntas['iPuntos'] = $rp->iPuntos;
                        $arr_preguntas['estatus_activo_pregunta'] = $rp->estatus_activo_pregunta;
                        $arr_preguntas['respuestas'] = [];
                        $reluclp_respuestas = new Reluclp_respuestas();

                        $reluclp_respuestas = $reluclp_respuestas->select(
                            'rel_uclp_respuestas.iId as id_respuesta',
                            'rel_uclp_respuestas.iFk_id_rel_ucl_preguntas',
                            'rel_uclp_respuestas.iFk_id_respuesta',
                            're.cRespuesta as respuesta',
                            're.iFk_id_pregunta',
                            're.iCorrecta',
                            're.iActivo as estatus_activo_respuesta'
                        );
                        $reluclp_respuestas = $reluclp_respuestas->join('respuestas as re','re.iId','=','rel_uclp_respuestas.iFk_id_respuesta');
                        
                        $reluclp_respuestas = $reluclp_respuestas->where('rel_uclp_respuestas.iFk_id_rel_ucl_preguntas',$rp->id_pregunta);
                        $reluclp_respuestas = $reluclp_respuestas->get();
                        if($reluclp_respuestas != null){
                            foreach ($reluclp_respuestas as $rr) {
                                $arr_respuesta = [];
                                $arr_respuesta['id_respuesta'] = $rr->id_respuesta;
                                $arr_respuesta['iFk_id_rel_ucl_preguntas'] = $rr->iFk_id_rel_ucl_preguntas;
                                $arr_respuesta['iFk_id_respuesta'] = $rr->iFk_id_respuesta;
                                $arr_respuesta['respuesta'] = $rr->respuesta;
                                $arr_respuesta['iFk_id_pregunta'] = $rr->iFk_id_pregunta;
                                $arr_respuesta['iCorrecta'] = $rr->iCorrecta;
                                $arr_respuesta['estatus_activo_respuesta'] = $rr->estatus_activo_respuesta;
                                array_push($arr_preguntas['respuestas'],$arr_respuesta);
                            }
                        }
                        array_push($arr_lecciones['preguntas'], $arr_preguntas);  
                    }
                }
                                
            }
        }
        array_push($lecciones_registradas_estudiante['lecciones'],$arr_lecciones);
        $response['success'] = true;
        $response['data']    = $lecciones_registradas_estudiante;
        $response['message'] = 'Consulta exitosa.';
        
        return $response;
    

    }

    /**
     * Responder la pregunta
     * @param  int  $id = id_pregunta .- del registro de la pregunta de la sesión [se envía en el listado general en cada registros]
     * @param array $respuestas = puede ser una, más de una pero no todas, todas [según la opción con las que creó la pregunta]
     */

    public function answerPregunta(Request $request){   
        $response = $this->response;
        $id_pregunta_registrada = 0;   
  
         if($request->input('id_pregunta') == null){
            $response['success'] = false;
            $response['message'] = 'Error! Se requiere de un id de pregunta para continuar.';            
            return $response;     
         }
         $id_pregunta_registrada = intval($request->input('id_pregunta'));         
         $pregunta_registrada = new Relucl_preguntas();
         $pregunta_registrada = $pregunta_registrada->find($id_pregunta_registrada);

         $opcion_multiple_de_pregunta = "";
         $puntos_pregunta = 0;
         $id_preg = 0;

         if($pregunta_registrada != null){
           
            $id_preg = $pregunta_registrada->iFk_id_pregunta;
            $pregunta = new Pregunta();
            $pregunta = $pregunta->find($id_preg);

            if($pregunta != null){
                
                $puntos_pregunta = $pregunta->iPuntos;
                
                //Caso1 bOpcion_multiple_una_resp
                if($pregunta->bOpcion_multiple_una_resp == 1){
                    $opcion_multiple_de_pregunta = "unique";
                }

                //Caso2 bOpcion_multiple_varias_resp
                	
                if($pregunta->bOpcion_multiple_varias_resp == 1){
                    $opcion_multiple_de_pregunta = "many";
                }
                //Caso3 bOpcion_multiple_todas_resp_correctas	
                
                if($pregunta->bOpcion_multiple_todas_resp_correctas == 1){
                    $opcion_multiple_de_pregunta = "all";
                }
            }
         }         
         
         $respuestas = $request->filled('respuestas') ? $request->input('respuestas') : null;         

         if(empty($respuestas)){            
            $response['success'] = false;
            $response['code'] = 400;
            $response['message'] = "Error! se requiere de una respuesta al menos para continuar";
            return response()->json($response);
         }

        try {
            //code...

            if($opcion_multiple_de_pregunta == "unique"){
                if(count($respuestas) > 1){
                    $response['success'] = false;
                    $response['code'] = 400;
                    $response['message'] = "Error! La respuesta a esta pregunta, solo requiere de una opción.";
                    return response()->json($response);
                }
                
                foreach ($respuestas as $r) {
                    // Se toma el id y se verifica la informacíon de la respuesta
                    // Se valida si el id coincide con el que debe ser correcta
                    // Si lo es ahora si vamos a usar los puntos de la pregunta
                    // $puntos_pregunta tienes los puntos de la pregunta
                    $resp_ = new Respuesta();
                    $resp_ = $resp_->where('iCorrecta',1);
                    $resp_ = $resp_->where('iFk_id_pregunta',$id_pregunta_registrada);
                    $resp_ = $resp_->first();
                    if($resp_ != null){
                        if($r['id_respuesta'] == $resp_->iId){
                            // es correcta y se va a actualizar el registro con 
                            // el valor de la pregunta
                            $ruclp = new Relucl_preguntas();
                            $ruclp = $ruclp->find($id_pregunta_registrada);
                            $ruclp->iResultado = $puntos_pregunta;
                            $ruclp->save();
                            $response['success'] = true;
                            $response['message'] = "Respuesta correcta";
                            $response['code']    = 200;
                        }
                    }
                }
            }
          
            if($opcion_multiple_de_pregunta == "many" || $opcion_multiple_de_pregunta == "all"){
                // Recupero las respuestas que deben de ser
               
                $resp_ = new Respuesta();       
                $resp_ = $resp_->select(
                    'respuestas.iId',
                    'respuestas.iFk_id_pregunta',
                    'respuestas.iCorrecta',
                    'ruclpr.iId as id_respuesta_registrada'
                );        
                $resp_ = $resp_->join('rel_uclp_respuestas as ruclpr','ruclpr.iFk_id_respuesta','=','respuestas.iId');
                $resp_ = $resp_->where('respuestas.iCorrecta',1);
                $resp_ = $resp_->where('respuestas.iFk_id_pregunta',$id_preg);
                $resp_ = $resp_->get();
                $contador = 0;

                if(count($resp_) == count($respuestas))
                {
                    // Aseguramos que el número es correcto+
                    // Confirmamos que sean los id's
                    // Recorro este arregalo y 
                    foreach ($resp_ as $r) {      
                                         
                        // voy comprando con los id 
                        // De los que llegaron en el arreglo
                        // $r->iId lo compararé con el del arreglo
                        foreach ($respuestas as $resp) {                                
                            if($r->id_respuesta_registrada == $resp['id_respuesta']){                                                       
                                $contador = $contador + 1;                             
                            }
                        }
                    }                           

                    if($contador == count($resp_) && $contador == count($respuestas))
                    {                        
                        // Todas coincidieron
                        // es correcta y se va a actualizar el registro con 
                        // el valor de la pregunta
                        $ruclp = new Relucl_preguntas();
                        $ruclp = $ruclp->find($id_pregunta_registrada);
                        $ruclp->iResultado = $puntos_pregunta;
                        $ruclp->save();

                        $response['success'] = true;
                        $response['message'] = "Respuesta correcta";
                        $response['code']    = 200;
                    }
                }
            }
        } catch (\Exception $e) {
            $response['success'] = false;
            $response['code'] = 400;
            $response['message'] = "Error! algo salió mal al querer responder la pregunta.";
            $response['error'] = $e->getMessage();
            return response()->json($response);
        }

        return response()->json($response);
    }

    public function endLeccion($id){
        // Aquí termina la lección y calcula todos los puntos
        // al mismo tiempo se valida si ya todas las secciones
        // están finalizadas y aprobadas y concluye el curso
        $response = $this->response;

        // Recibimos el id de la lección registrada que corresponde a 	iFk_id_rel_uc_lecciones de rel_ucl_preguntas
        if($id===0)
        {
            $response['success'] = false;
            $response['message'] = 'Error! Se requiere un id de lección diferente de cero (0)';            
            $response['code'] = 400;
            return response()->json($response,400);
        }

        // Obtenemos las preguntas de la lección
        $rel_preguntas = new Relucl_preguntas();
        $rel_preguntas = $rel_preguntas->where('iFk_id_rel_uc_lecciones', $id);
        $rel_preguntas = $rel_preguntas->get();

        if( $rel_preguntas == null ){
            $response['success'] = false;
            $response['message'] = 'Error! Algo salió mal, no se encontraron preguntas relacionadas con el registro de lección de estudiante con id = ' . $id;            
            $response['code'] = 400;
            return response()->json($response,400);
        }

        //Calcular los puntos: Se recorren todas las preguntas 
        $total_puntos_leccion = 0;
        foreach ($rel_preguntas as $p) {
            $total_puntos_leccion = $total_puntos_leccion + $p->iResultado;
        }

        //Validando los puntos de cada una de ellas
        // iPuntos de rel_uc_lecciones se actualiza conel puntaje
        $leccion_registrada = new Reluc_lecciones();
        $leccion_registrada = $leccion_registrada->find($id);

        if( $leccion_registrada == null ){
            $response['success'] = false;
            $response['message'] = 'Error! Algo salió mal, no se encontró la lección con id = ' . $id;            
            $response['code'] = 400;
            return response()->json($response,400);
        }

        // Obtenemos el mínimo aprobatorio de la lección
        $leccion = new Leccion();
        $leccion = $leccion->find($leccion_registrada->iFk_id_leccion);

        if($leccion === null){
            $response['success'] = false;
            $response['message'] = 'Error! Algo salió mal, no se encontraron los datos de la lección relacionada con el el registro de lección de estudiante con id = ' . $id;            
            $response['code'] = 400;
            return response()->json($response,400);
        }

        $puntos_aprobatorios_leccion = $leccion->iAprobacion;

        // Se actualiza los puntos 
        $leccion_registrada->iPuntos = $total_puntos_leccion;
        $leccion_registrada->iFinalizado = 1;

        if($total_puntos_leccion >= $puntos_aprobatorios_leccion ) {
            $leccion_registrada->iAprobado = 1;
        }

        $leccion_registrada->save();

        // Por último se revisa si el curso de la lección ya ha sido terminado
        // Verificando si todas las lecciones están finalizadas y si el puntaje es
        // Aprobatorio
        //iFk_id_rel_user_curso
        $lecciones_del_curso = new Reluc_lecciones();
        $lecciones_del_curso = $lecciones_del_curso->where('iFk_id_rel_user_curso', $leccion_registrada->iFk_id_rel_user_curso);
        $lecciones_del_curso = $lecciones_del_curso->get();

        $total_puntos_lecciones = 0;
        $finalizado = 1;
        $aprobado = 1;
        $sumatoria_puntos_lecciones = 0;
        foreach ($lecciones_del_curso as $lc) {
            if($lc->iFinalizado === 0){
                $finalizado = 0;
            }
            if($lc->iAprobado === 0){
                $aprobado = 0;
            }

            $sumatoria_puntos_lecciones = $sumatoria_puntos_lecciones + $lc->iPuntos;
        }

        if($finalizado ===0){
            $response['success'] = false;
            $response['message'] = 'Error! Para finalizar un curso, tienen que estar todos las lecciones finalizadas.';  
            $response['code'] = 400;
            return response()->json($response,400);
        }
        if($aprobado === 0){
            $response['success'] = false;
            $response['message'] = 'Error! Para finalizar un curso, tienen que estar aprobadas todas las lecciones';
            $response['code'] = 400;
            return response()->json($response,400);
        }

        $curso_registrado = new Relu_cursos();
        $curso_registrado = $curso_registrado->find($leccion_registrada->iFk_id_rel_user_curso);

        // $sumatoria_puntos_lecciones
        $curso_registrado->iPuntos = $sumatoria_puntos_lecciones;
        $curso_registrado->iFinalizado = 1;
        $curso_registrado->iAprobado = 1;
        $curso_registrado->save();

        $response['success'] = true;
        $response['message'] = 'Ok! Curso finalizado y aprobado';
        $response['code'] = 200;
        return response()->json($response,400);        

    }

}

