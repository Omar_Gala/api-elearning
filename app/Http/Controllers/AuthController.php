<?php

namespace Api\Http\Controllers;

use Illuminate\Http\Request;
use Api\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    //
      /**
     * Alta de usuario - En este caso no se usa
     */
    public function signup(Request $request)
    {
        $request->validate([
            'name'     => 'required|string',
            'email'    => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed',
            'id_perfil' => 'required|integer'
        ]);
        $user = new User([
            'name'     => $request->name,
            'email'    => $request->email,            
            'password' => bcrypt($request->password),
            'iFk_id_perfil'=> $request->id_perfil,
        ]);
        $user->save();
        return response()->json(['success' => true,
            'message' => 'Ok! Usuario creado satisfactoriamente!'], 201);
    }
    /**
     * Inicio de sesión del jugador
     * @param email
     * @param password
     * @param remember_me 
     * @return user
     */
    public function login(Request $request)
    {
        $request->validate([
            'email'       => 'required|string|email',
            'password'    => 'required|string',
            'remember_me' => 'boolean',
        ]);

        $credentials = request(['email', 'password']);        
        
        if (!Auth::attempt($credentials)) {
            return response()->json(['success' => false,
                'data'    => null,
                'message' => 'Ocurrió un error al intentar acceder a la aplicación.'], 401);
        }

        $user = $request->user();

        $tokenResult = $user->createToken('Personal Access Tokens');
        
        $token = $tokenResult->token;
        
        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }

        // Agregamos los datos de la tabla user que se requieren
        $user_ = [
            'id'    => $user->id,
            'name'  => $user->name,
            'email' => $user->email
        ];

        $token->save();
        return response()->json([
            'success' => true,
            'data'    => $user_,
            'message' => 'Ok! Acceso correcto.',
            'access_token' => $tokenResult->accessToken,
            'token_type'   => 'Bearer',
            'expires_at'   => Carbon::parse(
                $tokenResult->token->expires_at)
                    ->toDateTimeString(),
        ]);
    }

    /**
     * Cierra la sesión del jugador
     * @param $id de jugador.      
     * @return $user
     */
    public function logout(Request $request)
    {
        $user = Auth::user();        
        $request->user()->token()->revoke();
        return response()->json(['success' => true, 'message' => 
            'Ok! Cierre de sesión exitosa.']);
    }
    /**
     * Elimina al jugador y usuario.
     * @param $id de jugador.      
     * @return $user
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }


}
