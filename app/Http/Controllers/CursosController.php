<?php

namespace Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Api\Models\Curso;
use Api\Models\Leccion;

class CursosController extends Controller
{
    private $response = [
        'success' => false,
        'message' => "",
        'data'    => null,
        'code'  => "",
        'error'   => ""
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        //
        $response = $this->response;

        $cursos = Curso::all();
        
        if(count($cursos) > 0){
            $response['success'] = true;
            $response['message'] = "Consulta exitosa";
            $response['data'] = $cursos;
            $response['code'] = 200;
        }
        else
        {
            $response['success'] = true;
            $response['message'] = "Se devolvió una lista vacía";            
            $response['code'] = 200;
        }
   
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $response = $this->response;

        // Reglas de validación
        $rules = [
            'nombre' => 'bail|required|max:250',
            'nombre' => 'unique:mysql.cursos,cNombre'
        ];  
            
        // Instancia para validar en base a las reglas
        $validator = new Validator();

        // Obtiene el resultado de la validación
        // Si es true devuelve los errores
        $validator = $validator::make($request->all(), $rules);
        
        if ($validator->fails()) {
            $response['success'] = false;
            $response['message'] = 'Error! Algo salió mal al intentar crear un curso.';
            $response['error']   = $validator->errors()->all();
            return $response;            
        }
        try {            
            DB::beginTransaction();

            $curso = new Curso();
            $curso->cNombre = $request->input('nombre');
            $curso->save();

            $response['data'] = $curso; 
            $response['success'] = true;
            $response['message'] = "Los datos del curso se guardaron correctamente";
            $response['code'] = 200;                                  
            
        } catch (\Exception $e) {
            DB::rollBack();
            $response['success'] = false;
            $response['message'] = 'Error! Algo salió mal al intentar guardar los datos del curso.';
            $response['code'] = 300;
            $response['error']    = $e->getMessage();
        } finally {
            DB::commit();                      
        }        

        return response()->json($response, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $response = $this->response;
        //
        if($id===0)
        {
            $response['success'] = false;
            $response['message'] = 'Error! Se requiere un id de curso diferente de cero (0)';            
            $response['code'] = 400;
            return response()->json($response,400);
        }

        // devolver el curso con las lecciones que las conforman
        // informativo

        $curso = Curso::find($id);    

        if($curso == null){
            $response['success'] = false;
            $response['message'] = 'Error! No se encontró registro con id = '.$id;            
            $response['code'] = 400;
            return response()->json($response,400);
        }

        $lecciones = Leccion::where('iFk_id_curso',$id)->get();
        
        $response['success'] = true;
        $response['message'] = "Consulta exitosa";
        $response["code"] = 200;
        $response['data']['curso'] = $curso;
        $response['data']['lecciones'] = $lecciones;

        return response()->json($response,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $response = $this->response;

        // Reglas de validación
        $rules = [
            'nombre' => 'bail|required|max:250'
        ];  
            
        // Instancia para validar en base a las reglas
        $validator = new Validator();

        // Obtiene el resultado de la validación
        // Si es true devuelve los errores
        $validator = $validator::make($request->all(), $rules);

        if($id===0)
        {
            $response['success'] = false;
            $response['message'] = 'Error! Se requiere un id de curso diferente de cero (0)';            
            $response['code'] = 400;
        }
        
        if ($validator->fails()) {
            $response['success'] = false;
            $response['message'] = 'Error! Algo salió mal al intentar crear un curso.';
            $response['error']    = $validator->errors()->all();
            return $response;            
        }

        $curso = new Curso();
        $curso = $curso->where('cNombre',$request->input('nombre'));
        $curso = $curso->where('iId','!=',$id);
        $curso = $curso->first();

        if($curso != null || count($curso) > 0){
            $response['success'] = false;
            $response['message'] = 'Error! El campo nombre ya está en uso en otro registro.';
            $response['error']    = $validator->errors()->all();
            return $response;     
        }
        
        try {            
            DB::beginTransaction();

            $curso = new Curso();
            $curso = $curso->find($id);
            $curso->cNombre = $request->input('nombre');
            $curso->save();

            $response['data'] = $curso; 
            $response['success'] = true;
            $response['message'] = "Los datos del curso se guardaron correctamente";
            $response['code'] = 200;                                  
            
        } catch (\Exception $e) {
            DB::rollBack();
            $response['success'] = false;
            $response['message'] = 'Error! Algo salió mal al intentar guardar los datos del curso.';
            $response['code'] = 300;
            $response['error']    = $e->getMessage();
        } finally {
            DB::commit();                      
        }        

        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = $this->response;
        //
        if($id===0)
        {
            $response['success'] = false;
            $response['message'] = 'Error! Se requiere un id de curso diferente de cero (0)';            
            $response['code'] = 400;
            return response()->json($response,400);
        }

        $curso = Curso::find($id);
        if($curso == null){
            $response['success'] = false;
            $response['message'] = 'Error! No se enctró registro con id = '.$id;            
            $response['code'] = 400;
            return response()->json($response,400);
        }
        $curso->delete();
        
        $response['success'] = true;
        $response['message'] = "El registro ha sido borrado";
        $response["code"] = 200;
        $response['data'] = $curso;

        return response()->json($response,200);
    }
}
