<?php

namespace Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Api\Models\Leccion;
use Api\Models\Curso;

class LeccionesController extends Controller
{
    private $response = [
        'success' => false,
        'message' => "",
        'data'    => null,
        'code'  => "",
        'error'   => ""
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $response = $this->response;

        $lecciones = Leccion::all();
        
        if(count($lecciones) >0){
            $response['success'] = true;
            $response['message'] = "Consulta exitosa";
            $response['data'] = $lecciones;
            $response['code'] = 200;
        }
        else
        {
            $response['success'] = true;
            $response['message'] = "Se devolvió una lista vacía"; 
            $response['data'] = $lecciones;           
            $response['code'] = 200;
        }
   
        return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $response = $this->response;

        // Reglas de validación
        $rules = [
            'nombre' => 'bail|required|max:250',
            'nombre' => 'unique:mysql.lecciones,cNombre',
            'puntos_aprobacion' => 'bail|required|integer',
            'id_curso' => 'bail|required|integer'
        ];  
            
        // Instancia para validar en base a las reglas
        $validator = new Validator();

        // Obtiene el resultado de la validación
        // Si es true devuelve los errores
        $validator = $validator::make($request->all(), $rules);
        
        if ($validator->fails()) {
            $response['success'] = false;
            $response['message'] = 'Error! Algo salió mal al intentar crear un curso.';
            $response['error']    = $validator->errors()->all();
            return $response;            
        }
        try {            
            DB::beginTransaction();

            $leccion = new Leccion();
            $leccion->cNombre = $request->input('nombre');
            $leccion->iFk_id_curso = $request->input('id_curso');
            $leccion->iAprobacion = $request->input('puntos_aprobacion');
            $leccion->save();

            $response['data'] = $leccion; 
            $response['success'] = true;
            $response['message'] = "Los datos de la lección se guardaron correctamente";
            $response['code'] = 200;                                  
            
        } catch (\Exception $e) {
            DB::rollBack();
            $response['success'] = false;
            $response['message'] = 'Error! Algo salió mal al intentar guardar los datos de la lección.';
            $response['code'] = 300;
            $response['error']    = $e->getMessage();
        } finally {
            DB::commit();                      
        }        

        return response()->json($response, 200);
    }

    /**
     * Display the specified resource.
     * 
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         //
         $response = $this->response;
         //
         if($id===0)
         {
             $response['success'] = false;
             $response['message'] = 'Error! Se requiere un id de leccion diferente de cero (0)';            
             $response['code'] = 400;
             return response()->json($response,400);
         }
         $leccion = new Leccion();
         $leccion = $leccion->select('lecciones.iId',
            'lecciones.cNombre',
            'lecciones.iFk_id_curso',
            'c.cNombre as curso',
            'lecciones.iAprobacion',
            'lecciones.iActivo'
        );
         $leccion = $leccion->join('cursos as c','c.iId','=','lecciones.iFk_id_curso');
         $leccion = $leccion->where('lecciones.iId',$id);
         $leccion = $leccion->first();
 
        /*  $leccion = Leccion::find($id); */

         if($leccion == null){
             $response['success'] = false;
             $response['message'] = 'Error! No se encontró registro con id = '.$id;            
             $response['code'] = 400;
             return response()->json($response,400);
         }
         
         $response['success'] = true;
         $response['message'] = "Consulta exitosa";
         $response["code"] = 200;
         $response['data'] = $leccion;
 
         return response()->json($response,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $response = $this->response;

        // Reglas de validación
        $rules = [
            'nombre' => 'bail|required|max:250',            
            'puntos_aprobacion' => 'bail|required|integer',
            'id_curso' => 'bail|required|integer'
        ]; 
        // Instancia para validar en base a las reglas
        $validator = new Validator();

        // Obtiene el resultado de la validación
        // Si es true devuelve los errores
        $validator = $validator::make($request->all(), $rules);

        if($id===0)
        {
            $response['success'] = false;
            $response['message'] = 'Error! Se requiere un id de una lección diferente de cero (0)';            
            $response['code'] = 400;
        }
        
        if ($validator->fails()) {
            $response['success'] = false;
            $response['message'] = 'Error! Algo salió mal al intentar crear una lección.';
            $response['error']    = $validator->errors()->all();
            return $response;            
        }

        $leccion = new Leccion();
        $leccion = $leccion->where('cNombre',$request->input('nombre'));
        $leccion = $leccion->where('iFk_id_curso',$request->input('id_curso'));
        $leccion = $leccion->where('iId','!=',$id);
        $leccion = $leccion->first();

        if(count($leccion) > 0){
            $response['success'] = false;
            $response['message'] = 'Error! El campo nombre ya está en uso en otro registro.';
            $response['error']    = $validator->errors()->all();
            return $response;     
        }
        
        try {            
            DB::beginTransaction();

            $leccion = new Leccion();
            $leccion = $leccion->find($id);
            $leccion->cNombre = $request->input('nombre');
            $leccion->iAprobacion  = $request->input('puntos_aprobacion');
            $leccion->iFk_id_curso = $request->input('id_curso');
            $leccion->save();

            $response['data'] = $leccion; 
            $response['success'] = true;
            $response['message'] = "Los datos de la lección se guardaron correctamente";
            $response['code'] = 200;                                  
            
        } catch (\Exception $e) {
            DB::rollBack();
            $response['success'] = false;
            $response['message'] = 'Error! Algo salió mal al intentar guardar los datos de la leccion.';
            $response['code'] = 300;
            $response['error']    = $e->getMessage();
        } finally {
            DB::commit();                      
        }        

        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = $this->response;
        //
        if($id===0)
        {
            $response['success'] = false;
            $response['message'] = 'Error! Se requiere un id de curso diferente de cero (0)';            
            $response['code'] = 400;
            return response()->json($response,400);
        }

        $leccion = Leccion::find($id);
        if($leccion == null){
            $response['success'] = false;
            $response['message'] = 'Error! No se encontró registro con id = '.$id;            
            $response['code'] = 400;
            return response()->json($response,400);
        }
        $leccion->delete();
        
        $response['success'] = true;
        $response['message'] = "El registro ha sido borrado";
        $response["code"] = 200;
        $response['data'] = $leccion;

        return response()->json($response,200);
    }

}
