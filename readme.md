## Descarga e inicialización del proyecto modo DEV

Primer paso, clonar el proyecto en el equipo a trabajar:
`git clone https://Omar_Gala@bitbucket.org/Omar_Gala/api-elearning.git`

Ejecutamos el comando `composer install`

Al mismo tiempo, creamos una base de datos mysql desde el administrador de su preferencia. El nombre recomendado es "elearning". Ahora en la carpeta del proyecto ir a la carpeta database, donde encontrarán un archivo *.sql, el cual es la base de datos creada para trabajar y consumir la api, listo para importar. El nombre del archivo es: `elearning.bk.sql`

El siguiente paso es copiar el contenido del archivo .env.example en un nuevo archivo con el nombre .env. El archivo .env.example lo encuentras en la raíz del proyecto de Laravel. Pero espera, te tengo una solución más rápida y sencilla utilizando la Terminal. Dentro de la carpeta de tu proyecto ejecuta el siguiente comando: `cp .env.example .env`
 
En el archivo `.env` cambiar los parámetros de conexión a la base de datos de acuerdo a su configuración local. Estos son los siguientes:
- DB_CONNECTION=mysql
- DB_HOST=127.0.0.1 
- DB_PORT=3306
- DB_DATABASE=homestead   (nombre de la base de datos)
- DB_USERNAME=homestead   (usuario de la base de datos)
- DB_PASSWORD=secret      (password de la base de datos)

Generar APP_KEY y prueba. La APP_KEY es una cadena de carácteres generada aleatoriamente por Laravel que utiliza para todas las cookies cifradas, como las cookies de sesión. Para generar la APP_KEY del proyecto ejecuta el siguiente comando: `php artisan key:generate`

Ahora ejecuta servidor y abre la dirección en el navegador: `php artisan serve`

## Recursos 
Herramienta de consumo de api: POSTMAN

En el caso de `servidor` será el que de modo local haga referencia a su proyecto, en mi caso es: `http://localhost:8000/api/auth/login`

## Acceso de Usuario
Login: `http://servidor/api/auth/login`
Method: POST
- Existe un usuario administrador creado
Usuario: correo@dominio.com
Password: 12345678
- Existe un usuario de estudiante creado:
estudiante@hotmail.com
Password: 12345678

## Alta de Usuario
Agregar usuarios: `http://servidor/api/auth/signup`
Method: POST
- Headers:
* Accept - application/json
* Content-Type - application/x-www-form-urlencoded (este es necesario para el envío en modo formulario, puede omitirlo si desea hacerlo solo con JSON)
- Body:
- Para el alta se requieren:
    name
    email
    password
    password_confirmation
    id_perfil (Catálogo de perfiles: 1:administrador,2:profesor,3:alumno)

## Acceso de Usuario
Logout: `http://servidor/api/auth/logout`
Method: GET
Authorization: Bearer cadena de token devuelta en el login

## Información de Usuario
User: `http://servidor/api/auth/user`
Method: GET
Authorization: Bearer cadena de token devuelta en el login

## CURSOS
`http://servidor/api/cursos`

INDEX
Petición: GET
Params: N/A

STORE
Petición: POST
Params: nombre (nombre del curso)

SHOW
Petición: SHOW
Params: id (id del catálogo de cursos)

UPDATE
Petición: PUT
Params: nombre

DESTROY
Petición: DELETE
Params: id


## LECCIONES
`http://servidor/api/lecciones`
INDEX
Petición: GET
Params: N/A

STORE
Petición: POST
Params: nombre (nombre del curso)
nombre            => 'brequired|max:250|unique',
puntos_aprobacion => 'required|integer',
id_curso          => 'required|integer'

SHOW
Petición: SHOW
Params: id (id del catálogo de lecciones)

UPDATE
`http://servidor/api/lecciones/id`
id          => integer
Petición: PUT
Params: 
nombre            => 'brequired|max:250|unique',
puntos_aprobacion => 'required|integer',
id_curso          => 'required|integer'

DESTROY
Petición: DELETE
Params: id

## PREGUTNAS
`http://servidor/api/preguntas`
INDEX
Petición: GET
Params: N/A

STORE
Petición: POST
Params:
pregunta   => required|max:250|unique
id_leccion => required
puntos     => required|integer


SHOW
Petición: SHOW
Params: id (id del catálogo de preguntas)

UPDATE
`http://servidor/api/preguntas/id`
id          => integer
Petición: PUT
Params: 
pregunta   => required|max:250|unique
id_leccion => required
puntos     => required|integer

DESTROY
Petición: DELETE
Params: id

## RESPUESTAS
`http://servidor/api/respuestas`
INDEX
Petición: GET
Params: N/A

STORE
Petición: POST
Params: 
respuesta   => required|max:250
id_pregunta => required

SHOW
Petición: SHOW
Params: id (id del catálogo de cursos)

UPDATE
`http://servidor/api/respuestas/id`
Petición: PUT
Params: 
id          => integer
respuesta   => required|max:250
id_pregunta => required

DESTROY
Petición: DELETE
Params: id


## SESIÓN ESTUDIANTE
`http://servidor/api/session/estudiante`
Method: GET
Muestra Curso que solo puede ver el usuario

Method: POST
Params: id_curso (el id del curso registrado cuando el estudiante de registra en él)

## SESIÓN LECCIONES
`http://servidor/api/session/lecciones/id`
Method: GET
Params: id (id de la lección del curso registrado por el estudiante)
Obtener lecciones de curso registrado que puede ver el estudiante

## SESIÓN RESPONDER
`http://servidor/api/session/responder`

Method: POST
Params:
- id_pregunta: id de la pregunta de la lección registrada en el curso por el estudiante.
- respuestas: respuesta o lista de respuestas seleccionada por el estudiante según la opción de la pregunta si es respuesta única, multiple pero no todas ó todas.

## SESION FINALIAR LECCION
`http://servidor/api/session/finalizarleccion`

Method: GET
Params:
- id: id de la lección del curso registrado por el estudiante.


<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1100 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost you and your team's skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell).

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Cubet Techno Labs](https://cubettech.com)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[British Software Development](https://www.britishsoftware.co)**
- **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
- **[DevSquad](https://devsquad.com)**
- [UserInsights](https://userinsights.com)
- [Fragrantica](https://www.fragrantica.com)
- [SOFTonSOFA](https://softonsofa.com/)
- [User10](https://user10.com)
- [Soumettre.fr](https://soumettre.fr/)
- [CodeBrisk](https://codebrisk.com)
- [1Forge](https://1forge.com)
- [TECPRESSO](https://tecpresso.co.jp/)
- [Runtime Converter](http://runtimeconverter.com/)
- [WebL'Agence](https://weblagence.com/)
- [Invoice Ninja](https://www.invoiceninja.com)
- [iMi digital](https://www.imi-digital.de/)
- [Earthlink](https://www.earthlink.ro/)
- [Steadfast Collective](https://steadfastcollective.com/)
- [We Are The Robots Inc.](https://watr.mx/)
- [Understand.io](https://www.understand.io/)
- [Abdel Elrafa](https://abdelelrafa.com)

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
