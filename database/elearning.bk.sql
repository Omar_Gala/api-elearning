-- MySQL dump 10.13  Distrib 5.7.19, for Win64 (x86_64)
--
-- Host: localhost    Database: elearning
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cursos`
--

DROP TABLE IF EXISTS `cursos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cursos` (
  `iId` bigint(20) NOT NULL AUTO_INCREMENT,
  `cNombre` varchar(250) NOT NULL,
  `iActivo` tinyint(1) DEFAULT '1' COMMENT 'Campo status true = 1 o false = 0',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Campo fecha de creacionl',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Campo fecha de edicion',
  PRIMARY KEY (`iId`),
  UNIQUE KEY `cNombre` (`cNombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lecciones`
--

DROP TABLE IF EXISTS `lecciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lecciones` (
  `iId` bigint(20) NOT NULL AUTO_INCREMENT,
  `cNombre` varchar(250) NOT NULL,
  `iFk_id_curso` bigint(20) NOT NULL,
  `iAprobacion` int(11) NOT NULL,
  `iActivo` tinyint(1) DEFAULT '1' COMMENT 'Campo status true = 1 o false = 0',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Campo fecha de creacionl',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Campo fecha de edicion',
  PRIMARY KEY (`iId`),
  UNIQUE KEY `cNombre` (`cNombre`),
  KEY `FK_ID_CURSO` (`iFk_id_curso`),
  CONSTRAINT `FK_ID_CURSO` FOREIGN KEY (`iFk_id_curso`) REFERENCES `cursos` (`iId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_06_01_000001_create_oauth_auth_codes_table',1),(4,'2016_06_01_000002_create_oauth_access_tokens_table',1),(5,'2016_06_01_000003_create_oauth_refresh_tokens_table',1),(6,'2016_06_01_000004_create_oauth_clients_table',1),(7,'2016_06_01_000005_create_oauth_personal_access_clients_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` VALUES ('a82094dde25f24e9d82f12f75ff8536d5bd2b4e483b1cfbcabcce45d96cb1481252307f533f54ff1',1,3,NULL,'[]',0,'2020-04-03 06:21:49','2020-04-03 06:21:49','2021-04-03 00:21:49'),('c93de6d981be97fb05fbe40d3445c3c89a6165522fb8583ad4952b9ed7220eeb7927ed5a767be6ea',1,1,'Personal Access Tokens','[]',0,'2020-04-03 06:57:25','2020-04-03 06:57:25','2021-04-03 00:57:25'),('4a8ff06d6f5b426ee621510fd0d82c71c9f4a471eab2640f2f2807259344a8de5a2768fc3f0f5e2d',1,1,'Personal Access Tokens','[]',0,'2020-04-03 06:57:49','2020-04-03 06:57:49','2021-04-03 00:57:49'),('1b23764f8a5e3065e2a73c158a4eba90b82e42d1df1169c274b0cb9604a95065f0622c6740ef9736',1,1,'Personal Access Tokens','[]',1,'2020-04-03 06:58:07','2020-04-03 06:58:07','2021-04-03 00:58:07'),('e94b35ecd6f4b538874b2549afa4188532cb975baacaaa028199ea3f262897a44ee8d72307a3e929',1,1,'Personal Access Tokens','[]',0,'2020-04-03 07:45:57','2020-04-03 07:45:57','2021-04-03 01:45:57'),('1485e4ecfe8b288e2585268d9b40728b5058701ae20eee04247bf94add98453afd0f7b527614f730',1,1,'Personal Access Tokens','[]',0,'2020-04-03 07:50:29','2020-04-03 07:50:29','2021-04-03 01:50:29'),('b0fada5106ebb7ddf5f9b89fa564adbbea64e0754854996986674f3bf1fb6fac1e9c98b39c60545e',2,1,'Personal Access Tokens','[]',0,'2020-04-04 01:11:06','2020-04-04 01:11:06','2021-04-03 19:11:06');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_auth_codes`
--

LOCK TABLES `oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` VALUES (1,NULL,'Laravel Personal Access Client','h0dK8JKTluInrqBUs6cnGyyBU1oSxLIlTX3Q6KB0','http://localhost',1,0,0,'2020-04-03 05:26:17','2020-04-03 05:26:17');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_personal_access_clients`
--

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` VALUES (1,1,'2020-04-03 05:26:17','2020-04-03 05:26:17');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
INSERT INTO `oauth_refresh_tokens` VALUES ('27d078b622d46dc4b22807accf8f51562c14578f36c0b979a8004bbcbe78fa0a816f4a3649cb695c','a82094dde25f24e9d82f12f75ff8536d5bd2b4e483b1cfbcabcce45d96cb1481252307f533f54ff1',0,'2021-04-03 00:21:49');
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfiles`
--

DROP TABLE IF EXISTS `perfiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfiles` (
  `iId` bigint(20) NOT NULL AUTO_INCREMENT,
  `cPerfil` varchar(100) NOT NULL,
  PRIMARY KEY (`iId`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfiles`
--

LOCK TABLES `perfiles` WRITE;
/*!40000 ALTER TABLE `perfiles` DISABLE KEYS */;
INSERT INTO `perfiles` VALUES (1,'administrador'),(2,'profesor'),(3,'alumno');
/*!40000 ALTER TABLE `perfiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `preguntas`
--

DROP TABLE IF EXISTS `preguntas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `preguntas` (
  `iId` bigint(20) NOT NULL AUTO_INCREMENT,
  `cPregunta` varchar(250) NOT NULL,
  `iFk_id_leccion` bigint(20) NOT NULL,
  `bOpcion_multiple_una_resp` tinyint(1) DEFAULT '0',
  `bOpcion_multiple_varias_resp` tinyint(1) DEFAULT '0',
  `bOpcion_multiple_todas_resp_correctas` tinyint(1) DEFAULT '0',
  `iPuntos` int(11) NOT NULL,
  `iActivo` tinyint(1) DEFAULT '1' COMMENT 'Campo status true = 1 o false = 0',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Campo fecha de creacionl',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Campo fecha de edicion',
  PRIMARY KEY (`iId`),
  KEY `FK_ID_LECCION` (`iFk_id_leccion`),
  CONSTRAINT `FK_ID_LECCION` FOREIGN KEY (`iFk_id_leccion`) REFERENCES `lecciones` (`iId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rel_uc_lecciones`
--

DROP TABLE IF EXISTS `rel_uc_lecciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rel_uc_lecciones` (
  `iId` bigint(20) NOT NULL AUTO_INCREMENT,
  `iFk_id_rel_user_curso` bigint(20) NOT NULL,
  `iFk_id_leccion` bigint(20) NOT NULL,
  `iFinalizado` tinyint(1) DEFAULT '0' COMMENT 'Se vuelve verdadero, cuando concluye todas las preguntas',
  `iAprobado` tinyint(1) DEFAULT '0' COMMENT 'Se vuelve verdadero, cuando cuando termina de responder todas las preguntas pero además cumplió el mínimo aprobatorio',
  `iPuntos` int(11) DEFAULT '0' COMMENT 'Puntos acumulados en la leccion',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Campo fecha de creacionl',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Campo fecha de edicion',
  PRIMARY KEY (`iId`),
  KEY `FK_ID_LECCION_UC` (`iFk_id_leccion`),
  CONSTRAINT `FK_ID_LECCION_UC` FOREIGN KEY (`iFk_id_leccion`) REFERENCES `lecciones` (`iId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rel_ucl_preguntas`
--

DROP TABLE IF EXISTS `rel_ucl_preguntas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rel_ucl_preguntas` (
  `iId` bigint(20) NOT NULL AUTO_INCREMENT,
  `iFk_id_rel_uc_lecciones` bigint(20) NOT NULL,
  `iFk_id_pregunta` bigint(20) NOT NULL,
  `iResultado` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Campo fecha de creacionl',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Campo fecha de edicion',
  PRIMARY KEY (`iId`),
  KEY `FK_ID_REL_UC_LECCIONES` (`iFk_id_rel_uc_lecciones`),
  CONSTRAINT `FK_ID_REL_UC_LECCIONES` FOREIGN KEY (`iFk_id_rel_uc_lecciones`) REFERENCES `rel_uc_lecciones` (`iId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rel_uclp_respuestas`
--

DROP TABLE IF EXISTS `rel_uclp_respuestas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rel_uclp_respuestas` (
  `iId` bigint(20) NOT NULL AUTO_INCREMENT,
  `iFk_id_rel_ucl_preguntas` bigint(20) NOT NULL,
  `iFk_id_respuesta` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Campo fecha de creacionl',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Campo fecha de edicion',
  PRIMARY KEY (`iId`),
  KEY `FK_ID_REL_UCL_PREGUNTAS` (`iFk_id_rel_ucl_preguntas`),
  KEY `FK_ID_RESPUESTA` (`iFk_id_respuesta`),
  CONSTRAINT `FK_ID_REL_UCL_PREGUNTAS` FOREIGN KEY (`iFk_id_rel_ucl_preguntas`) REFERENCES `rel_ucl_preguntas` (`iId`),
  CONSTRAINT `FK_ID_RESPUESTA` FOREIGN KEY (`iFk_id_respuesta`) REFERENCES `respuestas` (`iId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rel_user_curso`
--

DROP TABLE IF EXISTS `rel_user_curso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rel_user_curso` (
  `iId` bigint(20) NOT NULL AUTO_INCREMENT,
  `iFk_id_usuario` bigint(20) NOT NULL,
  `ifk_id_curso` bigint(20) NOT NULL,
  `iFinalizado` tinyint(1) DEFAULT '0' COMMENT 'Se vuelve verdadero, cuando concluye todas las lecciones',
  `iAprobado` tinyint(1) DEFAULT '0' COMMENT 'Se vuelve verdadero, cuando cuando termina todas las lecciones pero con calificaion aprobatoria',
  `iPuntos` int(11) DEFAULT '0' COMMENT 'Puntos acumulados en el curso',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Campo fecha de creacionl',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Campo fecha de edicion',
  PRIMARY KEY (`iId`),
  KEY `FK_ID_CURSO_USER` (`ifk_id_curso`),
  CONSTRAINT `FK_ID_CURSO_USER` FOREIGN KEY (`ifk_id_curso`) REFERENCES `cursos` (`iId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `respuestas`
--

DROP TABLE IF EXISTS `respuestas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `respuestas` (
  `iId` bigint(20) NOT NULL AUTO_INCREMENT,
  `cRespuesta` varchar(250) NOT NULL,
  `iFk_id_pregunta` bigint(20) NOT NULL,
  `iCorrecta` tinyint(1) DEFAULT '0',
  `iActivo` tinyint(1) DEFAULT '1' COMMENT 'Campo status true = 1 o false = 0',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'Campo fecha de creacionl',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Campo fecha de edicion',
  PRIMARY KEY (`iId`),
  KEY `FK_ID_PREGUNTA` (`iFk_id_pregunta`),
  CONSTRAINT `FK_ID_PREGUNTA` FOREIGN KEY (`iFk_id_pregunta`) REFERENCES `preguntas` (`iId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `iFk_id_perfil` int(1) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'administrador','correo@dominio.com',1,NULL,'$2y$10$zfQgVx.QTnHGyBEFOMemoOUsCUmOeoXgYPt8s9mrJ0j8Fa5lZGRJe','euSMGSJbQW6AY4VBKeINFJsZV10OErAc6xxsPEBrOF5brFmJQlbjBkKticdP','2020-04-03 06:03:21','2020-04-03 06:03:21'),(2,'estudiante','estudiante@hotmail.com',3,NULL,'$2y$10$i9pTCMbz0rEX8B0MwSPozuQH.jzR9JEb/r7c2SMGxcM5zIaHo/9KO',NULL,'2020-04-04 00:54:16','2020-04-04 00:54:16');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-07 12:42:30
